<?php
$group = directory_value($data, 'group', []);
$location = directory_value($group, 'location', []);
$pageData = directory_value($data, 'people', []);
$people = directory_value($pageData, 'data', []);
?>
<div>
    <?php echo directory_value($group, 'name').' @ '.directory_value($location, 'full_name'); ?>
</div>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Job</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($people as $person): ?>
        <?php
        $intermediate = directory_get_primary_intermediate($person);
        $job = directory_value($intermediate, 'job', []);
        $name = directory_value($person, 'full_name');
        ?>
        <tr>
            <td>
                <?php echo l($name, 'directory/profile/'.directory_value($person, 'id')); ?>
            </td>
            <td><?php echo directory_value($job, 'full_name'); ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div>
    <?php $currentPage = directory_value($pageData, 'current_page', 1); ?>
    <?php if (directory_value($pageData, 'prev_page_url')): ?>
        <?php echo directory_group_link('Previous', $id, $currentPage - 1); ?>
    <?php endif; ?>
    <span class="active"><?php echo $currentPage; ?></span>
    <?php if (directory_value($pageData, 'next_page_url')): ?>
        <?php echo directory_group_link('Next', $id, $currentPage + 1); ?>
    <?php endif; ?>
</div>
